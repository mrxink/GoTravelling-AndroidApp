package com.teamwork.utils;

import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.net.ConnectivityManager;
import android.util.Log;
import com.android.volley.NetworkResponse;
import com.android.volley.ParseError;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.JsonRequest;
import com.android.volley.toolbox.StringRequest;
import com.teamwork.user.UserManager;

public class HttpUtil
{
	/**
	 * 以json串的post请求方式进行提交, 服务端返回json串
	 * @param requestQueue 请求队列对象
	 * @param url 目标URI
	 * @param method 请求的方法
	 * @param isLogin 是否是登录请求
	 * @param params 请求参数
	 * @param listener 监听回调
	 * @return 返回json数据
	 */
	public static void httpPostForJson(RequestQueue requestQueue, String url, int method, final boolean isLogin,
			Map<String, String> params, final HttpResponseListener listener)
	{
		final String cookie = params.get(UserManager.COOKIE);
		
		params.remove(UserManager.COOKIE);
		JSONObject jsonObject = new JSONObject(params);
		Log.d("weicong", "json -> " + jsonObject.toString());
		JsonRequest<JSONObject> jsonRequest = new JsonObjectRequest(method, url, jsonObject,
		    new Response.Listener<JSONObject>()
		    {
		        @Override
		        public void onResponse(JSONObject response)
		        {
		        	Log.d("weicong", "success -> " + response.toString());
		            listener.onResponse(response.toString());
		        }
		    }, 
		    new Response.ErrorListener() 
		    {
		        @Override
		        public void onErrorResponse(VolleyError error) 
		        {
		        	if (error.networkResponse != null)
		        	{
		        		byte[] htmlBodyBytes = error.networkResponse.data;
		        		listener.onErrorResponse(new String(htmlBodyBytes));
		   
		        		Log.e("weicong", "error -> " + new String(htmlBodyBytes));
		        	}
		        	else
		        	{
		        		Log.e("weicong", "message -> " + error.getMessage());
		        		Log.e("weicong", "cause -> " + error.getCause());
		        		Log.e("weicong", "class -> " + error.getClass());
		        		Log.e("weicong", "stackTrace -> " + error.getStackTrace());
		        		listener.onErrorResponse(null);
		        	}
		        }
		    })
		{     
		    @Override
		    public Map<String, String> getHeaders()
		    {
		    	HashMap<String, String> headers = new HashMap<String, String>();
		    	headers.put("Accept", "application/json");
		    	headers.put("Content-Type", "application/json; charset=UTF-8");
		    	if (!isLogin)
		    	{
		    		headers.put("Cookie", cookie);
		    	}
		                 
		    	return headers;
		    }
		    
		    @Override
		    protected Response<JSONObject> parseNetworkResponse(NetworkResponse response) 
		    {
		    	Log.d("weicong", "Status -> " + response.statusCode);
		    	Log.d("weicong", "headers -> " + response.headers.toString());
		    	Log.d("weicong", "data -> " + new String(response.data));
		    	
		    	try
		    	{
		    		String jsonString = new String(response.data, HttpHeaderParser.parseCharset(response.headers));
		    		String header = response.headers.toString();
		    		//使用正则表达式从reponse的头中提取cookie内容的子串
		    		String cookieFromResponse = null;
		    		Pattern pattern = Pattern.compile("Set-Cookie.*?;");
		    		Matcher m = pattern.matcher(header);
		    		if(m.find())
		    		{
		    			cookieFromResponse =m.group();
		    		}
		    		//去掉cookie末尾的分号
		    		cookieFromResponse = cookieFromResponse.substring(11, cookieFromResponse.length()-1);
//		    		Log.d("weicong", "Cookie -> " + cookieFromResponse);
		    		JSONObject jsonObject = new JSONObject(jsonString);
		    		jsonObject.put(UserManager.COOKIE, cookieFromResponse);
		    		//通知相应的修改成功
		    		
		    		return Response.success(jsonObject, HttpHeaderParser.parseCacheHeaders(response));
		    	}
		    	catch (UnsupportedEncodingException e)
		    	{
		    		return Response.error(new ParseError(e));
		    	}
		    	catch (JSONException je)
		    	{
		    		 return Response.error(new ParseError(je));
		    	}
		    }
		};
		
		requestQueue.add(jsonRequest);
	}
	
	/**
	 * 以jsonObject的post请求方式进行提交, 服务端返回json对象
	 * @param requestQueue 请求队列对象
	 * @param url 目标URI
	 * @param method 请求的方法
	 * @param cookie 
	 * @param jsonObject 请求jsonObject对象
	 * @param listener 监听回调
	 */
	public static void httpJson(RequestQueue requestQueue, String url, int method, final String cookie,
			JSONObject jsonObject, final HttpResponseListener listener)
	{
		Log.d("weicong", "json -> " + jsonObject.toString());
		JsonRequest<JSONObject> jsonRequest = new JsonObjectRequest(method, url, jsonObject,
		    new Response.Listener<JSONObject>()
		    {
		        @Override
		        public void onResponse(JSONObject response)
		        {
		        	Log.d("weicong", "success -> " + response.toString());
		            listener.onResponse(response.toString());
		        }
		    }, 
		    new Response.ErrorListener() 
		    {
		        @Override
		        public void onErrorResponse(VolleyError error) 
		        {
		        	if (error.networkResponse != null)
		        	{
		        		byte[] htmlBodyBytes = error.networkResponse.data;
		        		listener.onErrorResponse(new String(htmlBodyBytes));
		   
		        		Log.e("weicong", "error -> " + new String(htmlBodyBytes));
		        	}
		        	else
		        	{
		        		Log.e("weicong", "message -> " + error.getMessage());
		        		Log.e("weicong", "cause -> " + error.getCause());
		        		Log.e("weicong", "class -> " + error.getClass());
		        		Log.e("weicong", "stackTrace -> " + error.getStackTrace());
		        		listener.onErrorResponse(null);
		        	}
		        }
		    })
		{     
		    @Override
		    public Map<String, String> getHeaders()
		    {
		    	HashMap<String, String> headers = new HashMap<String, String>();
		    	headers.put("Accept", "application/json");
		    	headers.put("Content-Type", "application/json; charset=UTF-8");
		    	headers.put("Cookie", cookie);
		    	
		    	return headers;
		    }
		};
		
		requestQueue.add(jsonRequest);
	}
	
	/**
	 * 判断当前网络是否可用
	 * @param context
	 * @return
	 */
	public static boolean isNetworkAvailable(Context context)
	{
		ConnectivityManager cm = (ConnectivityManager)context.getSystemService(Context.CONNECTIVITY_SERVICE);
		if (cm.getActiveNetworkInfo() != null)
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	
	/**
	 * Http Get请求
	 * @param requestQueue 求队列对象
	 * @param url 目标URI
	 * @param method 请求的方法
	 * @param cookie
	 * @param listener 监听回调
	 */
	public static void httpGet(RequestQueue requestQueue, String url, int method,
			final String cookie, final HttpResponseListener listener)
	{
		StringRequest stringRequest = new StringRequest(method, url, 
		    new Response.Listener<String>()
		    {
		        @Override
		        public void onResponse(String response)
		        {
		        	Log.d("weicong", "success -> " + response.toString());
		            listener.onResponse(response.toString());
		        }
		    }, 
		    new Response.ErrorListener() 
		    {
		        @Override
		        public void onErrorResponse(VolleyError error) 
		        {
		        	if (error.networkResponse != null)
		        	{
		        		byte[] htmlBodyBytes = error.networkResponse.data;
		        		listener.onErrorResponse(new String(htmlBodyBytes));
		        		Log.e("weicong", "error -> " + new String(htmlBodyBytes));
		        	}
		        	else
		        	{
		        		Log.e("weicong", "error -> null");
		        		listener.onErrorResponse(null);
		        	}
		        }
		    })
		{       
		    @Override
		    public Map<String, String> getHeaders()
		    {
		    	HashMap<String, String> headers = new HashMap<String, String>();
		    	headers.put("Accept", "application/json");
		    	headers.put("Content-Type", "application/json; charset=UTF-8");
		    	headers.put("Cookie", cookie);
		    	
		    	return headers;
		    }
		    
		    @Override
		    protected Response<String> parseNetworkResponse(NetworkResponse response) 
		    {
		    	Log.d("weicong", "Status -> " + response.statusCode);
		    	return super.parseNetworkResponse(response);
		    }
		    
		};
		
		requestQueue.add(stringRequest);
	}
	
	public static void httpJsonGetForArray(RequestQueue requestQueue, String url,
			final String cookie, String params, final HttpResponseListener listener)
	{
		url = url + "?" + params;
		JsonArrayRequest jsonArrayRequest = new JsonArrayRequest(url,
		    new Response.Listener<JSONArray>()
		    {
		        @Override
		        public void onResponse(JSONArray response)
		        {
		        	Log.d("weicong", "success -> " + response.toString());
		            listener.onResponse(response.toString());
		        }
		    }, 
		    new Response.ErrorListener() 
		    {
		        @Override
		        public void onErrorResponse(VolleyError error) 
		        {
		        	if (error.networkResponse != null)
		        	{
		        		byte[] htmlBodyBytes = error.networkResponse.data;
		        		listener.onErrorResponse(new String(htmlBodyBytes));
		        		Log.e("weicong", "error -> " + new String(htmlBodyBytes));
		        	}
		        	else
		        	{
		        		Log.e("weicong", "error -> null");
		        		listener.onErrorResponse(null);
		        	}
		        }
		    })
		{       
		    @Override
		    public Map<String, String> getHeaders()
		    {
		    	HashMap<String, String> headers = new HashMap<String, String>();
		    	headers.put("Accept", "application/json");
		    	headers.put("Content-Type", "application/json; charset=UTF-8");
		    	headers.put("Cookie", cookie);
		    	
		    	return headers;
		    }
		    
		};
		
		requestQueue.add(jsonArrayRequest);
	}
}
