package com.teamwork.utils;

import android.content.Context;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnKeyListener;
import android.view.inputmethod.InputMethodManager;

public class MyOnKeyListener implements OnKeyListener 
{

	@Override
	public boolean onKey(View v, int keyCode, KeyEvent event) 
	{
		if(keyCode == KeyEvent.KEYCODE_ENTER)
		{  
			InputMethodManager imm = (InputMethodManager)v.getContext().getSystemService(Context.INPUT_METHOD_SERVICE);  
			  
			if(imm.isActive())
			{  
				imm.hideSoftInputFromWindow(v.getApplicationWindowToken(), 0 );
			}  
			return true;
		}
		return false;
	}
}
