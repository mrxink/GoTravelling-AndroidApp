package com.teamwork.utils;

public class HttpUrl
{
	public static final String HOME = "https://php-spatra.rhcloud.com"; 
	
	public static final String LOGIN = HOME + "/api/auth/login"; //登录的URI
	public static final String REGISTER = HOME + "/api/auth/register"; //注册的URI
	public static final String LOGOUT = HOME + "/api/auth/logout"; //登出的URI
	public static final String USER_INFO = HOME + "/api/personal/info"; //修改用户信息的URI
	public static final String HEAD_IMAGE = HOME + "/api/personal/head-image"; //修改用户头像的URI
	public static final String PASSWORD = HOME + "/api/personal/password"; //修改用户密码的URI
	public static final String PLACE_FAVORITE = HOME + "/api/collection"; //收藏地点的URI
	public static final String ROUTE_FAVORITE = HOME + "/api/collection-route"; //收藏路线的URI
	public static final String ROUTE = HOME + "/api/route"; //获取路线的URI
	public static final String IMAGE_HEADER = HOME + "/image/header"; //获取创建路线的头像
}
