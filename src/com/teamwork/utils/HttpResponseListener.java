package com.teamwork.utils;

public interface HttpResponseListener
{
	public void onResponse(String response);
	
	public void onErrorResponse(String error);
}
