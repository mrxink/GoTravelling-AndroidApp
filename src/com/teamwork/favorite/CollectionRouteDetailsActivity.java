package com.teamwork.favorite;

import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request.Method;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.Volley;
import com.android.volley.toolbox.ImageLoader.ImageListener;
import com.teamwork.gotravelling.R;
import com.teamwork.model.SearchRoute;
import com.teamwork.user.UserManager;
import com.teamwork.utils.BitmapCache;
import com.teamwork.utils.HandleUtil;
import com.teamwork.utils.HttpResponseListener;
import com.teamwork.utils.HttpUrl;
import com.teamwork.utils.HttpUtil;

public class CollectionRouteDetailsActivity extends Activity 
{

	public static final String ROUTE_INDEX = "route_index";
	
	private ImageView mHeadImage;
	
	private SearchRoute mRoute;
	
	private RequestQueue mQueue;
	private ProgressDialog mProgressDialog;
	
	private ImageLoader mImageLoader;

	@Override
	protected void onCreate(Bundle savedInstanceState) 
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_search_route_details);
		
		mQueue = Volley.newRequestQueue(this.getApplicationContext());
		mImageLoader = new ImageLoader(mQueue, new BitmapCache());
		mProgressDialog = HandleUtil.showProgressDialog(this, "正在获取路线详情信息...");
		
		showActionBar();
		Button collectionButton = (Button)findViewById(R.id.btn_favorite);
		collectionButton.setVisibility(View.GONE);
		
		int routeIndex = getIntent().getIntExtra(ROUTE_INDEX, 0);
		getRouteData(routeIndex);
	}
	
	private void showActionBar()
	{
		Button backButton = (Button)findViewById(R.id.btn_actionbar);
        backButton.setBackgroundResource(R.drawable.btn_back);
        backButton.setOnClickListener(new View.OnClickListener()
        {
			@Override
			public void onClick(View v) 
			{
				finish();
			}
		});
        
        TextView textView = (TextView)findViewById(R.id.tv_actionbar);
        textView.setText(R.string.route_details);
        
	}
	
	private void getRouteData(int routeIndex)
	{
		mProgressDialog.show();
		
		String cookie = UserManager.getInstance(this).getCookie();
		
		String url =  HttpUrl.ROUTE_FAVORITE + "/" + CollectionRouteManager.getInstance().getRoute(routeIndex).getId();
		HttpUtil.httpGet(mQueue, url, Method.GET, cookie, new ResponseListener());
	}
	
	private void init()
	{
		mHeadImage = (ImageView)findViewById(R.id.iv_head);
		
		TextView routeNameText = (TextView)findViewById(R.id.tv_route_name);
		routeNameText.setText(mRoute.getName());
		
		TextView routeDescriptionText = (TextView)findViewById(R.id.tv_route_description);
		routeDescriptionText.setText(mRoute.getDescription());
		
		TextView usernameText = (TextView)findViewById(R.id.tv_username);
		usernameText.setText(mRoute.getUsername());
		
		TextView createDateText = (TextView)findViewById(R.id.tv_create_date);
		String date = mRoute.getCreatedAt();
		createDateText.setText(date.substring(0, 10));
		
		String url =  HttpUrl.IMAGE_HEADER +  "/" + mRoute.getHeadImage();
		ImageListener listener = ImageLoader.getImageListener(mHeadImage, android.R.drawable.ic_menu_rotate, 0);  
		mImageLoader.get(url, listener);  
	}
	
	private class ResponseListener implements HttpResponseListener
	{

		@Override
		public void onResponse(String response)
		{
			mProgressDialog.dismiss();
			try
			{
				JSONObject jsonObject = new JSONObject(response);
				mRoute = new SearchRoute(jsonObject);
				init();
			}
			catch (JSONException e)
			{
				e.printStackTrace();
			}
		}

		@Override
		public void onErrorResponse(String error) 
		{
			mProgressDialog.dismiss();
			Toast.makeText(CollectionRouteDetailsActivity.this, "网络连接超时", Toast.LENGTH_SHORT).show();
			finish();
		}
	}
}
