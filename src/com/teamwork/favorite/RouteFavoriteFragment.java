package com.teamwork.favorite;

import java.util.ArrayList;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.ContextMenu;
import android.view.ContextMenu.ContextMenuInfo;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.AdapterContextMenuInfo;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request.Method;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;
import com.teamwork.gotravelling.R;
import com.teamwork.model.CollectionRoute;
import com.teamwork.user.UserManager;
import com.teamwork.utils.HandleUtil;
import com.teamwork.utils.HttpResponseListener;
import com.teamwork.utils.HttpUrl;
import com.teamwork.utils.HttpUtil;

public class RouteFavoriteFragment extends Fragment 
{
	private ListView mListView;
	private RouteAdapter mRouteAdapter;
	
	private int mCurrentPosition;
	
	private RequestQueue mQueue;
	
	private ProgressDialog mDeleteProgressDialog;
	
	@Override
	public void onCreate(Bundle savedInstanceState) 
	{
		super.onCreate(savedInstanceState);
		
		mQueue = Volley.newRequestQueue(getActivity().getApplicationContext());
		mDeleteProgressDialog = HandleUtil.showProgressDialog(getActivity(), "正在删除...");
		
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
	{
		View v = inflater.inflate(R.layout.fragment_route_favorite, null);
		mListView = (ListView)v.findViewById(R.id.lv_routes);
		mRouteAdapter = new RouteAdapter(CollectionRouteManager.getInstance().getRoutes());
		mListView.setAdapter(mRouteAdapter);
		mListView.setOnItemClickListener(new OnItemClickListener() 
		{
			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int position, long arg3) 
			{
				Intent intent = new Intent(getActivity(), CollectionRouteDetailsActivity.class);
				intent.putExtra(CollectionRouteDetailsActivity.ROUTE_INDEX, position);
				startActivity(intent);
			}
		});
		registerForContextMenu(mListView);
		if (mRouteAdapter.getCount() == 0)
		{
			Toast.makeText(getActivity(), "当前收藏的路线列表为空", Toast.LENGTH_SHORT).show();
		}
		
		return v;
	}
	
	@Override
	public void onCreateContextMenu(ContextMenu menu, View v, ContextMenuInfo menuInfo) 
	{
		getActivity().getMenuInflater().inflate(R.menu.route_list_item_context, menu);
	}
	
	@Override
	public boolean onContextItemSelected(MenuItem item) 
	{
		AdapterContextMenuInfo info = (AdapterContextMenuInfo)item.getMenuInfo();
		mCurrentPosition = info.position;
		switch (item.getItemId())
		{
		case R.id.menu_item_delete_route:
			mDeleteProgressDialog.show();
			deleteRoute();
			break;
		}
		return super.onContextItemSelected(item);
	}
	
	private void deleteRoute()
	{
		String cookie = UserManager.getInstance(getActivity()).getCookie();
			
		String uri = HttpUrl.ROUTE_FAVORITE + "/" + mRouteAdapter.getItem(mCurrentPosition).getId();
		HttpUtil.httpGet(mQueue, uri, Method.DELETE, cookie, new DeleteRouteResponseListener());
	}
	
	private class RouteAdapter extends ArrayAdapter<CollectionRoute>
	{
		public RouteAdapter(ArrayList<CollectionRoute> routes)
		{
			super(getActivity(), 0, routes);
		}
		
		@Override
		public View getView(int position, View convertView, ViewGroup parent)
		{
			if (convertView == null)
			{
				convertView = getActivity().getLayoutInflater().inflate(R.layout.personal_route_item, null);
			}
			
			CollectionRoute route = getItem(position);
			
			TextView text = (TextView)convertView.findViewById(R.id.tv_route_name);
			text.setText(route.getName());
			
			return convertView;
		}
	}
	
	private class DeleteRouteResponseListener implements HttpResponseListener
	{

		@Override
		public void onResponse(String response)
		{
			CollectionRouteManager.getInstance().removeRoute(mCurrentPosition);
			mRouteAdapter.notifyDataSetChanged();
			mDeleteProgressDialog.dismiss();
		}

		@Override
		public void onErrorResponse(String error) 
		{
			mDeleteProgressDialog.dismiss();
			Toast.makeText(getActivity(), "删除路线失败", Toast.LENGTH_SHORT).show();
		}
	}
}
