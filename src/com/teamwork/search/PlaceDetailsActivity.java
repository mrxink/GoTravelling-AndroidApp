package com.teamwork.search;

import org.json.JSONException;
import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.RequestQueue;
import com.android.volley.Request.Method;
import com.android.volley.toolbox.Volley;
import com.baidu.mapapi.search.poi.OnGetPoiSearchResultListener;
import com.baidu.mapapi.search.poi.PoiDetailResult;
import com.baidu.mapapi.search.poi.PoiDetailSearchOption;
import com.baidu.mapapi.search.poi.PoiResult;
import com.baidu.mapapi.search.poi.PoiSearch;
import com.teamwork.daily.SightShowMapActivity;
import com.teamwork.favorite.PlaceManager;
import com.teamwork.gotravelling.R;
import com.teamwork.model.Place;
import com.teamwork.model.Sight;
import com.teamwork.user.UserManager;
import com.teamwork.utils.HandleUtil;
import com.teamwork.utils.HttpResponseListener;
import com.teamwork.utils.HttpUrl;
import com.teamwork.utils.HttpUtil;

public class PlaceDetailsActivity extends Activity 
{

	private PoiSearch mPoiSearch = null;
	
	private String mUid;
	private String mName;
	
	private WebView mWebView;
	private Button mCollectionButton;
	
	private PoiDetailResult mPoiDetailResult;
	
	private ProgressDialog mCollectionDialog;
	
	private RequestQueue mQueue;
	
	@SuppressLint("SetJavaScriptEnabled") 
	@Override
	protected void onCreate(Bundle savedInstanceState) 
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_place_details);
		
		Intent intent = getIntent();
		mUid = intent.getStringExtra(Sight.UID);
		mName = intent.getStringExtra(Sight.NAME);
		
		showActionBar();
		
		mQueue = Volley.newRequestQueue(this.getApplicationContext());
		mCollectionDialog = HandleUtil.showProgressDialog(this, "正在收藏...");
		
		final ProgressBar progressBar = (ProgressBar)findViewById(R.id.progress_bar);
		progressBar.setMax(100);
		
		mWebView = (WebView)findViewById(R.id.web_view);
		mWebView.getSettings().setJavaScriptEnabled(true);
		mWebView.setWebViewClient(new WebViewClient()
		{
			public boolean shouldOverrideUrlLoading(WebView view, String url)
			{
				return false;
			}
		});
		mWebView.setWebChromeClient(new WebChromeClient()
		{
			@Override
			public void onProgressChanged(WebView view, int progress)
			{
				if (progress == 100)
				{
					progressBar.setVisibility(View.INVISIBLE);
				}
				else
				{
					progressBar.setVisibility(View.VISIBLE);
					progressBar.setProgress(progress);
				}
			}
		});
		
		mCollectionButton = (Button)findViewById(R.id.btn_favorite);
		mCollectionButton.setOnClickListener(new View.OnClickListener()
		{
			@Override
			public void onClick(View v) 
			{
				mCollectionDialog.show();
				try
				{
					JSONObject jsonObject = new JSONObject();
					jsonObject.put(Place.NAME, mPoiDetailResult.getName());
					jsonObject.put(Place.LONGITUDE, mPoiDetailResult.getLocation().longitude);
					jsonObject.put(Place.LATITUDE, mPoiDetailResult.getLocation().latitude);
					jsonObject.put(Place.ADDRESS, mPoiDetailResult.getAddress());
					jsonObject.put(Place.UID, mPoiDetailResult.getUid());
					
					String cookie = UserManager.getInstance(PlaceDetailsActivity.this).getCookie();
					
					HttpUtil.httpJson(mQueue, HttpUrl.PLACE_FAVORITE, Method.POST, cookie, jsonObject, new CollectionResponseListener());
				}
				catch (JSONException e)
				{
					e.printStackTrace();
				}
			}
		});
		
		mPoiSearch = PoiSearch.newInstance();
		mPoiSearch.setOnGetPoiSearchResultListener(new OnGetPoiSearchResultListener() 
		{
			
			@Override
			public void onGetPoiResult(PoiResult result) 
			{
				
			}
			
			@Override
			public void onGetPoiDetailResult(PoiDetailResult result)
			{
				mWebView.loadUrl(result.getDetailUrl());
				mPoiDetailResult = result;
				mCollectionButton.setEnabled(true);
			}
		});
		
		mPoiSearch.searchPoiDetail((new PoiDetailSearchOption()).poiUid(mUid));

	}
	
	@Override
	protected void onDestroy()
	{
		mPoiSearch.destroy();
		super.onDestroy();
	}
	
	private void showActionBar()
	{
		Button backButton = (Button)findViewById(R.id.btn_actionbar);
        backButton.setBackgroundResource(R.drawable.btn_back);
        backButton.setOnClickListener(new View.OnClickListener()
        {
			@Override
			public void onClick(View v) 
			{
				finish();
			}
		});
        
        TextView textView = (TextView)findViewById(R.id.tv_actionbar);
        textView.setText(mName);
        
        Button showButton = (Button)findViewById(R.id.btn_right);
        showButton.setVisibility(View.VISIBLE);
        showButton.setBackgroundResource(R.drawable.btn_show_map);
        showButton.setOnClickListener(new View.OnClickListener() 
        {
			@Override
			public void onClick(View v) 
			{
				Intent intent = new Intent(PlaceDetailsActivity.this, SightShowMapActivity.class);
				intent.putExtra(Sight.NAME, mPoiDetailResult.getName());
				intent.putExtra(Sight.LONGITUDE, mPoiDetailResult.getLocation().longitude);
				intent.putExtra(Sight.LATITUDE, mPoiDetailResult.getLocation().latitude);
				startActivity(intent);
			}
		});
	}
	
	private class CollectionResponseListener implements HttpResponseListener
	{

		@Override
		public void onResponse(String response)
		{
			try
			{
				JSONObject jsonObject = new JSONObject(response);
				Place place = new Place();
				place.setId(jsonObject.getString(Place.ID));
				place.setName(mPoiDetailResult.getName());
				place.setLongitude( mPoiDetailResult.getLocation().longitude);
				place.setLatitude(mPoiDetailResult.getLocation().latitude);
				place.setAddress(mPoiDetailResult.getAddress());
				place.setUid(mPoiDetailResult.getUid());
				PlaceManager.getInstance().addPlace(place);
			}
			catch (JSONException e)
			{
				e.printStackTrace();
			}
			mCollectionDialog.dismiss();
			finish();
		}

		@Override
		public void onErrorResponse(String error) 
		{
			mCollectionDialog.dismiss();
			Toast.makeText(PlaceDetailsActivity.this, "网络连接超时", Toast.LENGTH_SHORT).show();
		}
	}
}
