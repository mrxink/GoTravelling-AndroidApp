package com.teamwork.search;

import java.util.ArrayList;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.Toast;

import com.baidu.mapapi.search.core.PoiInfo;
import com.baidu.mapapi.search.core.SearchResult;
import com.baidu.mapapi.search.poi.OnGetPoiSearchResultListener;
import com.baidu.mapapi.search.poi.PoiCitySearchOption;
import com.baidu.mapapi.search.poi.PoiDetailResult;
import com.baidu.mapapi.search.poi.PoiResult;
import com.baidu.mapapi.search.poi.PoiSearch;
import com.teamwork.gotravelling.R;
import com.teamwork.model.Place;
import com.teamwork.model.Sight;
import com.teamwork.utils.HandleUtil;
import com.teamwork.utils.MyOnKeyListener;

public class SightSearchFragment extends Fragment 
{
	private AutoCompleteTextView mSearchText;
	private ArrayAdapter<String> mPlaceAdapter;
	private ArrayList<Place> mPlaces = new ArrayList<Place>();
	
	private PoiSearch mPoiSearch;
	
	private ArrayList<String> mListName = new ArrayList<String>();
	private ProgressDialog mSearchProgressDialog;
	
	@Override
	public void onCreate(Bundle savedInstanceState) 
	{
		super.onCreate(savedInstanceState);
		
		mSearchProgressDialog = HandleUtil.showProgressDialog(getActivity(), "正在搜索...");
		
		mPoiSearch = PoiSearch.newInstance();
		mPoiSearch.setOnGetPoiSearchResultListener(new OnGetPoiSearchResultListener() 
		{
			@Override
			public void onGetPoiResult(PoiResult result)
			{
				mSearchProgressDialog.dismiss();
				mPlaceAdapter.clear();
				mPlaces.clear();
				mListName.clear();
				if (result == null || result.error == SearchResult.ERRORNO.RESULT_NOT_FOUND) 
				{
					Toast.makeText(getActivity(), "未找到结果，请输入详细的景点信息", Toast.LENGTH_SHORT).show();
				}
				else if (result.error == SearchResult.ERRORNO.NO_ERROR) //成功返回结果
				{
					Place place;
					for (PoiInfo poiInfo : result.getAllPoi())
					{
						if (!mListName.contains(poiInfo.name))
						{
							place = new Place();
							place.setName(poiInfo.name);
							place.setCity(poiInfo.city);
							place.setAddress(poiInfo.address);
							place.setLongitude(poiInfo.location.longitude);
							place.setLatitude(poiInfo.location.latitude);
							place.setUid(poiInfo.uid);
						
							mPlaces.add(place);
							mPlaceAdapter.add(poiInfo.name);
							mListName.add(poiInfo.name);
						}
					}
					mPlaceAdapter.notifyDataSetChanged();
					mSearchText.setText(mListName.get(0));
					mSearchText.setSelection(mListName.get(0).length());
				}
				else
				{
					Toast.makeText(getActivity(), "未找到结果，请输入详细的景点信息", Toast.LENGTH_SHORT).show();
				}
			}
			
			@Override
			public void onGetPoiDetailResult(PoiDetailResult result) 
			{
				
			}
		});
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) 
	{
		View v = inflater.inflate(R.layout.fragment_sight_search, container, false);
		
		mSearchText = (AutoCompleteTextView)v.findViewById(R.id.tv_search);
		mPlaceAdapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_dropdown_item_1line);
		mSearchText.setAdapter(mPlaceAdapter);
		mSearchText.setOnKeyListener(new MyOnKeyListener());
		
		Button searchButton = (Button)v.findViewById(R.id.btn_search);
		searchButton.setOnClickListener(new View.OnClickListener() 
		{
			@Override
			public void onClick(View v) 
			{
				hideSoftInput();
				String text = mSearchText.getText().toString();
				if (HandleUtil.isEmpty(text))
				{
					Toast.makeText(getActivity(), "搜索字符串为空", Toast.LENGTH_SHORT).show();
				}
				else
				{
					mSearchProgressDialog.show();
					mPoiSearch.searchInCity((new PoiCitySearchOption())
						.city("全国")
						.keyword(text)
						.pageCapacity(3));
				}
			}
		});
		
		mSearchText.setOnItemClickListener(new OnItemClickListener() 
		{
			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int position, long arg3) 
			{
				Intent intent = new Intent(getActivity(), PlaceDetailsActivity.class);
				Place place = mPlaces.get(position);
				intent.putExtra(Sight.UID, place.getUid());
				intent.putExtra(Sight.NAME, place.getName());
				startActivity(intent);
			}
		});
		
		return v;
	}
	
	@Override
	public void onDestroy()
	{
		mPoiSearch.destroy();
		super.onDestroy();
	}
	
	private void hideSoftInput()
	{
		InputMethodManager inputMethodManager = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE); 
		if(inputMethodManager.isActive())
		{  
			inputMethodManager.hideSoftInputFromWindow(mSearchText.getApplicationWindowToken(), 0 );
		}  
	}
}
