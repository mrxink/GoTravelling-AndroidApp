package com.teamwork.search;

import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request.Method;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.ImageLoader.ImageListener;
import com.android.volley.toolbox.Volley;
import com.teamwork.favorite.CollectionRouteManager;
import com.teamwork.gotravelling.R;
import com.teamwork.model.CollectionRoute;
import com.teamwork.model.SearchRoute;
import com.teamwork.user.UserManager;
import com.teamwork.utils.BitmapCache;
import com.teamwork.utils.HandleUtil;
import com.teamwork.utils.HttpResponseListener;
import com.teamwork.utils.HttpUrl;
import com.teamwork.utils.HttpUtil;

public class SearchRouteDetailsActivity extends Activity 
{
	public static final String ROUTE_INDEX = "route_index";
	
	private ImageView mHeadImage;
	
	private SearchRoute mRoute;
	
	private RequestQueue mQueue;
	private ProgressDialog mCollectionProgressDialog;

	private ImageLoader mImageLoader;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) 
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_search_route_details);
		
		mQueue = Volley.newRequestQueue(this.getApplicationContext());
		mImageLoader = new ImageLoader(mQueue, new BitmapCache());
		mCollectionProgressDialog = HandleUtil.showProgressDialog(this, "正在收藏...");
		
		showActionBar();
		
		int routeIndex = getIntent().getIntExtra(ROUTE_INDEX, 0);
		mRoute = SearchRouteManager.getInstance().getRoute(routeIndex);
		
		mHeadImage = (ImageView)findViewById(R.id.iv_head);
		
		TextView routeNameText = (TextView)findViewById(R.id.tv_route_name);
		routeNameText.setText(mRoute.getName());
		
		TextView routeDescriptionText = (TextView)findViewById(R.id.tv_route_description);
		routeDescriptionText.setText(mRoute.getDescription());
		
		TextView usernameText = (TextView)findViewById(R.id.tv_username);
		usernameText.setText(mRoute.getUsername());
		
		TextView createDateText = (TextView)findViewById(R.id.tv_create_date);
		String date = mRoute.getCreatedAt();
		createDateText.setText(date.substring(0, 10));
		
		Button collectionButton = (Button)findViewById(R.id.btn_favorite);
		collectionButton.setOnClickListener(new View.OnClickListener()
		{
			@Override
			public void onClick(View v)
			{
				collectionRoute();
			}
		});
		
		String url =  HttpUrl.IMAGE_HEADER +  "/" + mRoute.getHeadImage();
		ImageListener listener = ImageLoader.getImageListener(mHeadImage, android.R.drawable.ic_menu_rotate, 0);  
		mImageLoader.get(url, listener);  
	}
	
	private void showActionBar()
	{
		Button backButton = (Button)findViewById(R.id.btn_actionbar);
        backButton.setBackgroundResource(R.drawable.btn_back);
        backButton.setOnClickListener(new View.OnClickListener()
        {
			@Override
			public void onClick(View v) 
			{
				finish();
			}
		});
        
        TextView textView = (TextView)findViewById(R.id.tv_actionbar);
        textView.setText(R.string.route_details);
        
	}
	
	private void collectionRoute()
	{
		try
		{
			mCollectionProgressDialog.show();
			JSONObject jsonObject = new JSONObject();
			jsonObject.put("route_id", mRoute.getId());
			String cookie = UserManager.getInstance(this).getCookie();
			
			HttpUtil.httpJson(mQueue, HttpUrl.ROUTE_FAVORITE, Method.POST, cookie, jsonObject, new CollectionResponseListener());
			
		}
		catch (JSONException e)
		{
			e.printStackTrace();
		}
		
	}
	
	private class CollectionResponseListener implements HttpResponseListener
	{

		@Override
		public void onResponse(String response)
		{
			try
			{
				JSONObject jsonObject = new JSONObject(response);
				CollectionRoute route = new CollectionRoute(jsonObject);
				CollectionRouteManager.getInstance().addRoute(route);
			}
			catch (JSONException e)
			{
				e.printStackTrace();
			}
			mCollectionProgressDialog.dismiss();
			finish();
		}

		@Override
		public void onErrorResponse(String error) 
		{
			mCollectionProgressDialog.dismiss();
			if (HandleUtil.isEmpty(error))
			{
				Toast.makeText(SearchRouteDetailsActivity.this, "网络连接超时", Toast.LENGTH_SHORT).show();
			}
			else
			{
				Toast.makeText(SearchRouteDetailsActivity.this, "收藏失败，因为你是该路线的创建者", Toast.LENGTH_SHORT).show();
			}
		}
	}
}
