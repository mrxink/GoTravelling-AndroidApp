package com.teamwork.search;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;
import com.teamwork.gotravelling.R;
import com.teamwork.model.SearchRoute;
import com.teamwork.user.UserManager;
import com.teamwork.utils.HandleUtil;
import com.teamwork.utils.HttpResponseListener;
import com.teamwork.utils.HttpUrl;
import com.teamwork.utils.HttpUtil;

public class RouteSearchFragment extends Fragment 
{
	private EditText mSearchText;
	private ListView mListView;
	private SearchRouteAdapter mSearchRouteAdapter;
	
	private CheckBox mCheckBox;
	
	private ProgressDialog mSearchProgressDialog;
	
	private RequestQueue mQueue;
	SearchRouteManager mSearchRouteManager;
	
	@Override
	public void onCreate(Bundle savedInstanceState) 
	{
		super.onCreate(savedInstanceState);
		
		mQueue = Volley.newRequestQueue(getActivity().getApplicationContext());
		mSearchProgressDialog = HandleUtil.showProgressDialog(getActivity(), "正在搜索...");
		mSearchRouteManager = SearchRouteManager.getInstance();
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) 
	{
		View v = inflater.inflate(R.layout.fragment_route_search, container, false);
		mSearchText = (EditText)v.findViewById(R.id.tv_search);
		
		Button searchButton = (Button)v.findViewById(R.id.btn_search);
		searchButton.setOnClickListener(new View.OnClickListener()
		{
			@Override
			public void onClick(View v) 
			{
				hideSoftInput();
				String text = mSearchText.getText().toString();
				if (HandleUtil.isEmpty(text))
				{
					Toast.makeText(getActivity(), "搜索字符串为空", Toast.LENGTH_SHORT).show();
				}
				else
				{
					search(text, false);
					mCheckBox.setChecked(false);
				}
			}
		});
		
		mCheckBox = (CheckBox)v.findViewById(R.id.cb_all_routes);
		mCheckBox.setOnCheckedChangeListener(new OnCheckedChangeListener() 
		{
			@Override
			public void onCheckedChanged(CompoundButton arg0, boolean state)
			{
				if (state)
				{
					search(null, true);
				}
			}
		});
		
		mListView = (ListView)v.findViewById(R.id.lv_routes);
		mSearchRouteAdapter = new SearchRouteAdapter(mSearchRouteManager.getRoutes());
		mListView.setAdapter(mSearchRouteAdapter);
		mListView.setOnItemClickListener(new OnItemClickListener() 
		{
			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int position, long arg3)
			{
				Intent intent = new Intent(getActivity(), SearchRouteDetailsActivity.class);
				intent.putExtra(SearchRouteDetailsActivity.ROUTE_INDEX, position);
				startActivity(intent);
			}
		});
		
		return v;
	}
	
	@Override
	public void onDestroy() 
	{
		mSearchRouteManager.clear();
		super.onDestroy();
	}
	private void search(String searchText, boolean searchAll)
	{
		mSearchProgressDialog.show();
		String params;
		
		if (searchAll)
		{
			params = "type=latest";
		}
		else
		{
			params = "type=keyword&query=" + searchText;
		}
		
		String cookie = UserManager.getInstance(getActivity()).getCookie();
			
		HttpUtil.httpJsonGetForArray(mQueue, HttpUrl.ROUTE, cookie, params, new SearchRouteResponseListener());
		
	}
	
	private void hideSoftInput()
	{
		InputMethodManager inputMethodManager = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE); 
		if(inputMethodManager.isActive())
		{  
			inputMethodManager.hideSoftInputFromWindow(mSearchText.getApplicationWindowToken(), 0 );
		}  
	}
	
	private void parseJsonToSearchRoute(String jsonString)
	{
		mSearchRouteManager.clear();
		try
		{
			JSONArray jsonArray = new JSONArray(jsonString);
			JSONObject jsonObject;
			SearchRoute route;
			if (jsonArray.length() <= 0)
			{
				Toast.makeText(getActivity(), "未找到相关路线", Toast.LENGTH_SHORT).show();
				return;
			}
			for (int i = 0; i < jsonArray.length(); i++)
			{
				jsonObject = jsonArray.getJSONObject(i);
				route = new SearchRoute(jsonObject);
				
				mSearchRouteManager.addRoute(route);
			}
			mSearchRouteAdapter.notifyDataSetChanged();
		}
		catch (JSONException e)
		{
			e.printStackTrace();
		}
	}
	
	private class SearchRouteAdapter extends ArrayAdapter<SearchRoute>
	{
		public SearchRouteAdapter(ArrayList<SearchRoute> searchRoutes)
		{
			super(getActivity(), 0, searchRoutes);
		}
		
		@Override
		public View getView(int position, View convertView, ViewGroup parent)
		{
			if (convertView == null)
			{
				convertView = getActivity().getLayoutInflater().inflate(R.layout.personal_route_item, null);
			}
			
			SearchRoute route = getItem(position);
			TextView nameTextView = (TextView)convertView.findViewById(R.id.tv_route_name);
			nameTextView.setText(route.getName());
			
			return convertView;
		}
	}
	
	private class SearchRouteResponseListener implements HttpResponseListener
	{

		@Override
		public void onResponse(String response)
		{
			mSearchProgressDialog.dismiss();
			parseJsonToSearchRoute(response);
		}

		@Override
		public void onErrorResponse(String error) 
		{
			mSearchProgressDialog.dismiss();
			Toast.makeText(getActivity(), "网络连接超时", Toast.LENGTH_SHORT).show();
		}
	}
}
