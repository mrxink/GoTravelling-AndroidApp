package com.teamwork.user;

import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;

import android.app.Activity;
import android.content.ContentResolver;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.graphics.PointF;
import android.graphics.Rect;
import android.net.Uri;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.widget.Button;
import android.widget.ImageView;

import com.teamwork.gotravelling.R;
import com.teamwork.view.ClipView;

public class ClipPictureActivity extends Activity implements OnTouchListener
{
	public static final String SRC_PICTURE_PATH = "src_picture_path";
	
	// 定义3种模式
	private static final int NONE = 0; 
	private static final int DRAG = 1; //移动
	private static final int ZOOM = 2; //缩放
		
	private ImageView mSrcPictureView;
	private ClipView mClipView;
	
	private Matrix mCurrentMatrix = new Matrix(); //当前矩阵
	private Matrix mSavedMatrix = new Matrix(); //保存矩阵

	private int mCurrentMode = NONE; //记录当前的模式

	private PointF mStart = new PointF(); //记录起初的位置
	private PointF mMid = new PointF(); //记录中点的位置
	private float mOldDistance = 1f; //保存一开始两点间的距离
	
	private int mActionBarHeight = 50; //操作栏的高度
	private int mStatusBarHeight = 0; //状态栏的高度
	
	@Override
	public void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_clip_picture);

		mSrcPictureView = (ImageView) this.findViewById(R.id.iv_head);
		mSrcPictureView.setOnTouchListener(this);
		
		Intent intent = getIntent();
		Uri uri = intent.getData();
		ContentResolver cr = this.getContentResolver();
		Bitmap bitmap = null;
		try 
		{  
			//在装入内存前对其进行采样以节约内存
			BitmapFactory.Options options = new BitmapFactory.Options();
			options.inSampleSize = 2;
			bitmap = BitmapFactory.decodeStream(cr.openInputStream(uri), null, options);
			
		}
		catch (FileNotFoundException e)
		{
			e.printStackTrace();
		}
		mSrcPictureView.setImageBitmap(bitmap);
		
		showActionBar();
	}

	@Override
	public boolean onTouch(View v, MotionEvent event)
	{
		ImageView view = (ImageView) v;
		//多点触控
		switch (event.getAction() & MotionEvent.ACTION_MASK)
		{
			case MotionEvent.ACTION_DOWN:
				mSavedMatrix.set(mCurrentMatrix);
				//設置初始點位置
				mStart.set(event.getX(), event.getY());
				mCurrentMode = DRAG;
				break;
			case MotionEvent.ACTION_POINTER_DOWN:
				mOldDistance = distance(event);
				if (mOldDistance > 10f)
				{
					mSavedMatrix.set(mCurrentMatrix);
					setMidPoint(mMid, event);
					mCurrentMode = ZOOM;
				}
				break;
			case MotionEvent.ACTION_UP:
			case MotionEvent.ACTION_POINTER_UP:
				mCurrentMode = NONE;
				break;
			case MotionEvent.ACTION_MOVE:
				if (mCurrentMode == DRAG)
				{
					mCurrentMatrix.set(mSavedMatrix);
					mCurrentMatrix.postTranslate(event.getX() - mStart.x, event.getY()
							- mStart.y);
				}
				else if (mCurrentMode == ZOOM)
				{
					float newDistance = distance(event);
					if (newDistance > 10f)
					{
						mCurrentMatrix.set(mSavedMatrix);
						float scale = newDistance / mOldDistance;
						mCurrentMatrix.postScale(scale, scale, mMid.x, mMid.y);
					}
				}
				break;
			}

		view.setImageMatrix(mCurrentMatrix);
		return true; 
	}

	private void showActionBar()
	{
		mActionBarHeight = dipToPx(50);
		Button backButton = (Button)findViewById(R.id.btn_actionbar);
        backButton.setBackgroundResource(R.drawable.btn_back);
        backButton.setOnClickListener(new View.OnClickListener()
        {
			@Override
			public void onClick(View v) 
			{
				finish();
			}
		});
        
        
        Button okButton = (Button)findViewById(R.id.btn_right);
        okButton.setVisibility(View.VISIBLE);
        okButton.setBackgroundResource(R.drawable.btn_ok);
        okButton.setOnClickListener(new View.OnClickListener()
        {
			@Override
			public void onClick(View v) 
			{
				clipPicture();
			}
		});
	}
	
	//计算两点之间的距离
	private float distance(MotionEvent event)
	{
		float x = event.getX(0) - event.getX(1);
		float y = event.getY(0) - event.getY(1);
		return (float)Math.sqrt(x * x + y * y);
	}

	//设置两个点的中点
	private void setMidPoint(PointF point, MotionEvent event)
	{
		float x = event.getX(0) + event.getX(1);
		float y = event.getY(0) + event.getY(1);
		point.set(x / 2, y / 2);
	}

	
	private void clipPicture()
	{
		Bitmap bitmap = getBitmap();
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		bitmap.compress(Bitmap.CompressFormat.JPEG, 100, out);
		byte[] bitmapByte = out.toByteArray();

		Intent intent = new Intent();
		intent.putExtra(UserInfoActivity.HEAD_BITMAP_BYTE, bitmapByte);
		
		setResult(RESULT_OK, intent);
		finish();
	}

	//获取矩形区域内的截图
	private Bitmap getBitmap()
	{
		getStatusBarHeight();
		Bitmap screenShoot = takeScreenShot();
	
		mClipView = (ClipView)this.findViewById(R.id.clip_view);
		int width = mClipView.getWidth();
		int height = mClipView.getHeight();
		Bitmap finalBitmap = Bitmap.createBitmap(screenShoot,
				(width - height / 2) / 2, height / 4 + mActionBarHeight + mStatusBarHeight, height / 2, height / 2);
		return finalBitmap;
	}

	private void getStatusBarHeight()
	{
		//获取状态栏高度
		Rect frame = new Rect();
		this.getWindow().getDecorView().getWindowVisibleDisplayFrame(frame);
		mStatusBarHeight = frame.top;
	}

	//获取Activity的截屏
	private Bitmap takeScreenShot()
	{
		View view = this.getWindow().getDecorView();
		view.setDrawingCacheEnabled(true);
		view.buildDrawingCache();
		return view.getDrawingCache();
	}
	
	//根据手机的分辨率从 dp 的单位 转成为 px(像素)
	private int dipToPx(int dpValue) 
	{
		 final float scale = getResources().getDisplayMetrics().density;
		 return (int) (dpValue * scale + 0.5f);
	}
}
