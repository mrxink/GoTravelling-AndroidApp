package com.teamwork.gotravelling;

import java.util.ArrayList;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.baidu.location.BDLocation;
import com.baidu.location.BDLocationListener;
import com.baidu.location.LocationClient;
import com.baidu.location.LocationClientOption;
import com.baidu.location.LocationClientOption.LocationMode;
import com.teamwork.daily.PersonalRouteActivity;
import com.teamwork.favorite.CollectionRouteManager;
import com.teamwork.favorite.FavoriteActivity;
import com.teamwork.favorite.PlaceManager;
import com.teamwork.model.User;
import com.teamwork.search.SearchActivity;
import com.teamwork.sign.SignInActivity;
import com.teamwork.user.UserInfoActivity;
import com.teamwork.user.UserManager;
import com.teamwork.utils.HandleUtil;
import com.teamwork.view.ChildViewPager;
import com.teamwork.view.ImagePagerAdapter;

public class MainActivity extends Activity
{
	public static final String FROM_MAIN_ACTIVITY = "from_main_activity";
	public static final String LOCATION_NAME = "location_name";
	public static final String LONGITUDE = "longitude";
	public static final String LATITUDE = "latitude";
	
	private DrawerLayout mDrawerLayout; //DrawerLayout容器
	private RelativeLayout mMenuLayoutLeft; //左边抽屉
	private ListView mListView;
	
	private ChildViewPager viewPager;
	private ImageView[] tips; //tip的ImageView数组
	private ImageView[] mImageViews; //轮播图片
	private int[] imgResourceIdArray; //图片资源id
	private ArrayList<MyMenuItem> mMenus = new ArrayList<MyMenuItem>();
	
	private ViewGroup group;
	
	//定位功能
	private LocationClient mLocationClient;
	
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        
        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        mMenuLayoutLeft = (RelativeLayout)findViewById(R.id.menu_layout_left);
        
        mListView = (ListView)mMenuLayoutLeft.findViewById(R.id.lv_items);
        addMenu();
        mListView.setAdapter(new MenuAdapter(mMenus));
        mListView.setOnItemClickListener(new MyOnItemClickListener());
        
        Button drawerMenu = (Button)findViewById(R.id.btn_actionbar);
        drawerMenu.setBackgroundResource(R.drawable.btn_drawer_menu);
        drawerMenu.setOnClickListener(new View.OnClickListener()
        {
			@Override
			public void onClick(View v) 
			{
				mDrawerLayout.openDrawer(mMenuLayoutLeft);
			}
		});
        
        TextView textView = (TextView)findViewById(R.id.tv_actionbar);
        textView.setText(R.string.logo_text);
        
        Button searchButton = (Button)findViewById(R.id.btn_right);
        searchButton.setVisibility(View.VISIBLE);
        searchButton.setBackgroundResource(R.drawable.btn_search);
        searchButton.setOnClickListener(new View.OnClickListener() 
        {
			@Override
			public void onClick(View v) 
			{
				Intent intent = new Intent(MainActivity.this, SearchActivity.class);
				startActivity(intent);
			}
		});
        
        setViewPageImage(); 
        
        setUserInfo();
        
        //定位
        getLocation();
        
    }

    @Override
    public void onResume()
    {
    	super.onResume();
    	
    	setUserInfo();
    }
    
    @Override
    public void onStop()
    {
    	super.onStop();
    	if (mDrawerLayout != null)
    	{
    		mDrawerLayout.closeDrawer(mMenuLayoutLeft);
    	}
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) 
    {
    	return super.onCreateOptionsMenu(menu);
    }
    
    //初始化用户信息
	private void setUserInfo()
    {
    	 ImageView headView = (ImageView)findViewById(R.id.iv_head);
         User user = UserManager.getInstance(this).getUser();
         headView.setImageBitmap(user.getHeadImage());
         headView.setOnClickListener(new MyUserInfoClickListener());
         
    	TextView nickNameText = (TextView)findViewById(R.id.tv_username);
        String nickname = user.getNickname();
        if (HandleUtil.isEmpty(nickname))
        {
        	nickname = "请设置你的昵称";
        }
        nickNameText.setText(nickname);
        nickNameText.setOnClickListener(new MyUserInfoClickListener());
        
        TextView signatureText = (TextView)findViewById(R.id.tv_signature);
        String signature = user.getSignature();
        if (HandleUtil.isEmpty(signature))
        {
        	signature = "请设置你的个性签名！";
        }
        signatureText.setText(signature);
        signatureText.setOnClickListener(new MyUserInfoClickListener());
    }
    
    private class MyUserInfoClickListener implements OnClickListener
    {
    	@Override
		public void onClick(View v) 
		{
			openUserInfoActivity();
		}
    }
    
    //添加菜单
    private void addMenu()
    {
    	mMenus.add(new MyMenuItem(R.drawable.person, "个人信息"));
    	mMenus.add(new MyMenuItem(R.drawable.storage, "收藏夹"));
    	mMenus.add(new MyMenuItem(R.drawable.my_daily, "我的日程"));
    	mMenus.add(new MyMenuItem(R.drawable.sign_in_menu,"景点签到"));
    	mMenus.add(new MyMenuItem(R.drawable.settings, "设置"));
    	mMenus.add(new MyMenuItem(R.drawable.exit, "退出账号"));
    }
    
    //设置轮转播放图片
    private void setViewPageImage() 
    {
		group = (ViewGroup)findViewById(R.id.viewGroup);
		viewPager = (ChildViewPager)findViewById(R.id.viewPager);
		imgResourceIdArray = new int[] 
		{ 
				R.drawable.image1, R.drawable.image2,
				R.drawable.image4, R.drawable.image5, 
				R.drawable.image3 
		};
		
		initTip();
		
		mImageViews = new ImageView[imgResourceIdArray.length];
		for (int i = 0; i < mImageViews.length; i++) 
		{
			ImageView imageView = new ImageView(this);
			mImageViews[i] = imageView;
			imageView.setBackgroundResource(imgResourceIdArray[i]);
		}
		
		viewPager.setAdapter(new ImagePagerAdapter(mImageViews));
		viewPager.setOnPageChangeListener(new ViewPager.SimpleOnPageChangeListener()
		{
			@Override
			public void onPageSelected(int arg0)
			{
				setImageBackground(arg0 % mImageViews.length);
			}
		});
		//设置ViewPager的默认项, 设置为长度的100倍，这样子开始就能往左滑动
		viewPager.setCurrentItem(imgResourceIdArray.length * 100);
	}
    
    //设置刚进来时轮播图片的状态，设置第一张图片被选中
    private void initTip() 
    {
		tips = new ImageView[imgResourceIdArray.length];
		for (int i = 0; i < tips.length; i++) 
		{
			ImageView imageView = new ImageView(this);
			imageView.setLayoutParams(new LayoutParams(10, 10));
			tips[i] = imageView;
			if (i == 0) // 第一个点为选定状态
			{ 
				tips[i].setBackgroundResource(R.drawable.im_default_dot_down);
			} 
			else 
			{
				tips[i].setBackgroundResource(R.drawable.im_default_dot_up);
			}
			
			LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(
					new ViewGroup.LayoutParams(
							LayoutParams.WRAP_CONTENT,
							LayoutParams.WRAP_CONTENT));
			// 设置每个点之间的间隔距离
			layoutParams.leftMargin = 5;
			layoutParams.rightMargin = 5;
			group.addView(imageView, layoutParams);
		}
    }
    
    private class MyMenuItem
    {
    	public int icon;
    	public String title;
    	
    	public MyMenuItem(int icon, String title)
    	{
    		this.icon = icon;
    		this.title = title;
    	}
    }
    
    private class MenuAdapter extends ArrayAdapter<MyMenuItem>
    {
    	public MenuAdapter(ArrayList<MyMenuItem> menus)
    	{
    		super(MainActivity.this, 0, menus);
    	}
    	
    	@Override
    	public View getView(int position, View convertView, ViewGroup parent)
    	{
    		if (convertView == null)
    		{
    			convertView = getLayoutInflater().inflate(R.layout.drawer_list_item, null);
    		}
    		
    		MyMenuItem menuItem = getItem(position);
    		
    		ImageView iconImageView = (ImageView)convertView.findViewById(R.id.iv_icon);
    		iconImageView.setBackgroundResource(menuItem.icon);
    		TextView titleTextView = (TextView)convertView.findViewById(R.id.tv_title);
    		titleTextView.setText(menuItem.title);
    		
    		return convertView;
    	}
    }
    
    private class MyOnItemClickListener implements OnItemClickListener
    {

		@Override
		public void onItemClick(AdapterView<?> adapterView, View v, int position, long id)
		{
			Intent intent;
			switch (position)
			{
			case 0:
				openUserInfoActivity();
				break;
			case 1:
				intent = new Intent(MainActivity.this, FavoriteActivity.class);
				startActivity(intent);
				break;
			case 2:
				intent = new Intent(MainActivity.this, PersonalRouteActivity.class);
				startActivity(intent);
				break;
			case 3:
				intent = new Intent(MainActivity.this, SignInActivity.class);
				startActivity(intent);
				break;
			case 4:
				break;
			case 5:
				exitUserAccount();
				break;
			}
		}
    	
    }
    
    //设置选中的tip的背景
    private void setImageBackground(int selectItems) 
    {
		for (int i = 0; i < tips.length; i++)
		{
			if (i == selectItems) 
			{
				tips[i].setBackgroundResource(R.drawable.im_default_dot_down);
			} 
			else 
			{
				tips[i].setBackgroundResource(R.drawable.im_default_dot_up);
			}
		}
	}

    //打开个人中心
    private void openUserInfoActivity()
    {
    	Intent intent = new Intent(MainActivity.this, UserInfoActivity.class);
		startActivity(intent);
    }

    //退出账号
    private void exitUserAccount()
    {
    	PlaceManager.getInstance().reset();
    	CollectionRouteManager.getInstance().reset();
    	UserManager.getInstance(this).exitUserAccount();
    	Intent intent = new Intent(this, LoginActivity.class);
    	intent.putExtra(FROM_MAIN_ACTIVITY, true);
    	startActivity(intent);
    	finish();
    }
    
    //利用百度定位功能获取位置信息
    private void getLocation()
    {
    	mLocationClient = new LocationClient(this.getApplicationContext());
    	mLocationClient.registerLocationListener(new MyLocationListener());
    	
    	//初始化location
    	LocationClientOption option = new LocationClientOption();
		option.setLocationMode(LocationMode.Hight_Accuracy); //设置定位模式
		option.setCoorType("bd09ll"); //返回的定位结果是百度经纬度，默认值gcj02
		option.setIsNeedAddress(true);
		mLocationClient.setLocOption(option);
		
		//开始定位
    	mLocationClient.start();
    }
    
    public class MyLocationListener implements BDLocationListener
	{
		@Override
		public void onReceiveLocation(BDLocation location) 
		{
			
			SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(MainActivity.this);
			SharedPreferences.Editor editor = sharedPreferences.edit();
			editor.putString(LOCATION_NAME, location.getAddrStr());
			editor.putFloat(LONGITUDE, (float)location.getLongitude());
			editor.putFloat(LATITUDE, (float)location.getLatitude());
			editor.apply();
			
			mLocationClient.stop();
		}
	}
}
