package com.teamwork.gotravelling;

import java.util.HashMap;
import java.util.Map;

import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.Request.Method;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;
import com.teamwork.favorite.CollectionRouteManager;
import com.teamwork.favorite.PlaceManager;
import com.teamwork.model.User;
import com.teamwork.user.UserManager;
import com.teamwork.utils.HandleUtil;
import com.teamwork.utils.HttpResponseListener;
import com.teamwork.utils.HttpUrl;
import com.teamwork.utils.HttpUtil;
import com.teamwork.utils.MyOnKeyListener;

public class LoginActivity extends Activity 
{
	private EditText mUsernameText;
	private EditText mPasswordText;
	private RequestQueue mQueue; 
	
	private ProgressDialog mProgressDialog;
	
	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_login);
		
		mUsernameText = (EditText)findViewById(R.id.et_username);
		mPasswordText = (EditText)findViewById(R.id.et_password);
		mPasswordText.setOnKeyListener(new MyOnKeyListener());
		
		fromMainActivity();
		
		mQueue = Volley.newRequestQueue(this.getApplicationContext());
		
		Button loginButton = (Button)findViewById(R.id.btn_login);
		loginButton.setOnClickListener(new View.OnClickListener() 
		{
			@Override
			public void onClick(View v) 
			{
				login();
			}
		});
		
		Button backButton = (Button)findViewById(R.id.btn_back);
		backButton.setOnClickListener(new View.OnClickListener() 
		{
			@Override
			public void onClick(View v) 
			{
				back();
			}
		});
		
		mProgressDialog = HandleUtil.showProgressDialog(this, "正在登陆...");
	}
	
	private void fromMainActivity()
	{
		Intent intent = getIntent();
		boolean isFromMainActivity = intent.getBooleanExtra(MainActivity.FROM_MAIN_ACTIVITY, false);
		if (isFromMainActivity)
		{
			Map<String, String> params = UserManager.getInstance(this).getUserAccount();
			mUsernameText.setText(params.get("identify"));
			mPasswordText.setText(params.get("password"));
		}
	}
	
	//登录
	private void login()
	{
		Map<String, String> params = new HashMap<String, String>();
		String username = mUsernameText.getText().toString();
		String password = mPasswordText.getText().toString();
		if (validate(username, password))
		{
			mProgressDialog.show();
			if (HandleUtil.isPhoneNumber(username))
			{
				params.put("identify", username);
			}
			else
			{
				params.put("identify", username);
			}
			params.put("password", password);
			HttpUtil.httpPostForJson(mQueue, HttpUrl.LOGIN, Method.POST, true, params, new LoginResponseListener());
		}
	}
	
	private class LoginResponseListener implements HttpResponseListener
	{

		@Override
		public void onResponse(String response)
		{
			handleResult(response);
			getCollections();
		}

		@Override
		public void onErrorResponse(String error) 
		{
			handleError(error);
		}
		
	}
	
	private void handleError(String error)
	{
		Log.d("weicong", "error -> " + error);
		mProgressDialog.dismiss();
		if (error == null)
		{
			Toast.makeText(this, "网络连接超时", Toast.LENGTH_SHORT).show();
		}
		else
		{
			Toast.makeText(this, "登录失败，用户名（或手机号码）或密码错误！", Toast.LENGTH_SHORT).show();
		}
	}
	
	//处理结果
	private void handleResult(String result)
	{
		try
		{
			JSONObject json = new JSONObject(result);
			
			String nickname = json.getString(User.NICKNAME);
			String username = json.getString(User.USERNAME);
			String cellPhoneNumber = json.getString(User.CELL_PHONE_NUMBER);
			String sex = json.getString(User.SEX);
			String headImageBytes = json.getString(User.HEAD_IMAGE);
			String email = json.getString(User.EMAIL);
			String signature = json.getString(User.SIGNATURE);
			String cookie = json.getString(UserManager.COOKIE);
			UserManager.getInstance(this).setCookie(cookie);
			
			String[] data = headImageBytes.split("base64,");
			Bitmap bitmap = HandleUtil.base64ToBitmap(data[1]);
			
			User user = new User();
			user.setNickname(nickname);
			user.setUsername(username);
			user.setCellPhone(cellPhoneNumber);
			user.setHeadImage(bitmap);
			user.setSex(sex);
			user.setEmail(email);
			user.setSignature(signature);
			
			UserManager.getInstance(this).setUser(user);
			UserManager.getInstance(this).saveUserAccount(mUsernameText.getText().toString(), mPasswordText.getText().toString());
		}
		catch (JSONException e)
		{
			e.printStackTrace();
		}
	}
	
	//登录之前验证输入
	private boolean validate(String username, String password)
	{
		//用户名为空
		if (HandleUtil.isEmpty(username))
		{
			Toast.makeText(this, "请输入用户名或手机号！", Toast.LENGTH_SHORT).show();
			return false;
		}
		if (HandleUtil.isEmpty(password))
		{
			Toast.makeText(this, "请输入密码！", Toast.LENGTH_SHORT).show();
			return false;
		}
		if (!HandleUtil.validateLength(username))
		{
			Toast.makeText(this, "请输入6到20位的用户名！", Toast.LENGTH_SHORT).show();
			return false;
		}
		if (!HandleUtil.validateLength(password))
		{
			Toast.makeText(this, "请输入6到20位的密码！", Toast.LENGTH_SHORT).show();
			return false;
		}
		return true;
	}
	
	//后退
	private void back()
	{
		Intent intent = new Intent(this, StartActivity.class);
		startActivity(intent);
		finish();
	}
	
	@Override
	public void onBackPressed() 
	{
		back();
	}
	
	private void getCollectionRoute()
	{
		String cookie = UserManager.getInstance(this).getCookie();
		
		HttpUtil.httpGet(mQueue, HttpUrl.ROUTE_FAVORITE, Method.GET, cookie, new GetCollectionRouteResponseListener());
	}
	
	private void getCollections()
	{
		String cookie = UserManager.getInstance(this).getCookie();
		
		HttpUtil.httpGet(mQueue, HttpUrl.PLACE_FAVORITE, Method.GET, cookie, new GetCollectionResponseListener());
	}
	
	private class GetCollectionResponseListener implements HttpResponseListener
	{

		@Override
		public void onResponse(String response)
		{
			PlaceManager.getInstance().parseJsonToPlaces(response);
			getCollectionRoute();
		}

		@Override
		public void onErrorResponse(String error) 
		{
			mProgressDialog.dismiss();
			if (error == null)
			{
				Toast.makeText(LoginActivity.this, "网络连接超时", Toast.LENGTH_SHORT).show();
			}
		}
	}
	
	private class GetCollectionRouteResponseListener implements HttpResponseListener
	{

		@Override
		public void onResponse(String response)
		{
			CollectionRouteManager.getInstance().parseJsonToRoutes(response);
			Intent intent = new Intent(LoginActivity.this, MainActivity.class);
			startActivity(intent);
			mProgressDialog.dismiss();
			finish();
		}

		@Override
		public void onErrorResponse(String error)
		{
			mProgressDialog.dismiss();
			if (error == null)
			{
				Toast.makeText(LoginActivity.this, "网络连接超时", Toast.LENGTH_SHORT).show();
			}
		}
	}
}
