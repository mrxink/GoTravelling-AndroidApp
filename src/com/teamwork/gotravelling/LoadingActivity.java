package com.teamwork.gotravelling;
import java.util.Map;

import org.json.JSONException;
import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.provider.Settings;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Toast;

import com.android.volley.Request.Method;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;
import com.teamwork.favorite.CollectionRouteManager;
import com.teamwork.favorite.PlaceManager;
import com.teamwork.model.User;
import com.teamwork.user.UserManager;
import com.teamwork.utils.HandleUtil;
import com.teamwork.utils.HttpResponseListener;
import com.teamwork.utils.HttpUrl;
import com.teamwork.utils.HttpUtil;
public class LoadingActivity extends Activity 
{
	private boolean mIsSaveUserAccount = false;
	private RequestQueue mQueue;
	private Handler mHandler;
	
	@SuppressLint("HandlerLeak")
	@Override
	public void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
		setContentView(R.layout.activity_loading);
		
		mQueue = Volley.newRequestQueue(this.getApplicationContext()); 
	
		mHandler = new Handler()
		{
			@Override
			public void handleMessage(Message msg)
			{
				if (msg.what == 0x123)
				{
					redirectToStartActivity();
				}
				else if (msg.what == 0x124)
				{
					redirectToMainActivity();
				}
			}
		};
		
		if (!HttpUtil.isNetworkAvailable(this)) //当前无网络
		{
			handleNetwork();
		}
		else
		{
			isSaveUserAccount();
		}
	}
	
	//处理网络问题
	private void handleNetwork()
	{
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setMessage(R.string.has_no_network);
		builder.setPositiveButton(R.string.exit, new DialogInterface.OnClickListener() 
		{
			@Override
			public void onClick(DialogInterface dialog, int which)
			{
				finish();
			}
		});
		builder.setNegativeButton(R.string.settings, new DialogInterface.OnClickListener() 
		{
			@Override
			public void onClick(DialogInterface dialog, int which)
			{
				Intent intent = new Intent(Settings.ACTION_SETTINGS);
				startActivity(intent);
				finish();
			}
		});
		builder.setCancelable(false);
		builder.create().show();
	}
	
	private void redirectToStartActivity()
	{
		Intent intent = new Intent(this, StartActivity.class);
		startActivity(intent);
		finish();
	}
	
	private void redirectToMainActivity()
	{
		Intent intent = new Intent(this, MainActivity.class);
		startActivity(intent);
		finish();
	}
	
	//判断是否保存了账号
	private void isSaveUserAccount()
	{
		UserManager userManager = UserManager.getInstance(this);
		mIsSaveUserAccount = userManager.isSaveUserAccount();
		if (mIsSaveUserAccount)
		{
			Map<String, String> params = userManager.getUserAccount();
			HttpUtil.httpPostForJson(mQueue, HttpUrl.LOGIN, Method.POST, true, params, new LoginResponseListener());
		}
		else
		{
			mHandler.sendEmptyMessageDelayed(0x123, 1500);
		}
	}
	
	private class LoginResponseListener implements HttpResponseListener
	{

		@Override
		public void onResponse(String response)
		{
			handleResult(response);
			getCollections();
		}

		@Override
		public void onErrorResponse(String error) 
		{
			if (error == null)
			{
				Toast.makeText(LoadingActivity.this, "网络连接超时", Toast.LENGTH_SHORT).show();
			}
			else
			{
				Toast.makeText(LoadingActivity.this, "登录失败，用户名（或手机号码）或密码错误！", Toast.LENGTH_SHORT).show();
			}
			Intent intent = new Intent(LoadingActivity.this, StartActivity.class);
			startActivity(intent);
			finish();
		}	
	}
	
	//处理成功登录的结果
	private void handleResult(String result)
	{
		try
		{
			JSONObject json = new JSONObject(result);
			String nickname = json.getString(User.NICKNAME);
			String username = json.getString(User.USERNAME);
			String cellPhoneNumber = json.getString(User.CELL_PHONE_NUMBER);
			String sex = json.getString(User.SEX);
			String headImageBytes = json.getString(User.HEAD_IMAGE);
			String email = json.getString(User.EMAIL);
			String signature = json.getString(User.SIGNATURE);
			String cookie = json.getString(UserManager.COOKIE);
			UserManager.getInstance(this).setCookie(cookie);
			
			String[] data = headImageBytes.split("base64,");
			Bitmap bitmap = HandleUtil.base64ToBitmap(data[1]);
			
			User user = new User();
			user.setNickname(nickname);
			user.setUsername(username);
			user.setCellPhone(cellPhoneNumber);
			user.setHeadImage(bitmap);
			user.setSex(sex);
			user.setEmail(email);
			user.setSignature(signature);
			UserManager.getInstance(this).setUser(user);
		}
		catch (JSONException e)
		{
			e.printStackTrace();
		}
	}
	
	private void getCollectionRoute()
	{
		String cookie = UserManager.getInstance(this).getCookie();
		
		HttpUtil.httpGet(mQueue, HttpUrl.ROUTE_FAVORITE, Method.GET, cookie, new GetCollectionRouteResponseListener());
	}
	
	private void getCollections()
	{
		String cookie = UserManager.getInstance(this).getCookie();
		
		HttpUtil.httpGet(mQueue, HttpUrl.PLACE_FAVORITE, Method.GET, cookie, new GetCollectionResponseListener());
	}
	
	private class GetCollectionResponseListener implements HttpResponseListener
	{

		@Override
		public void onResponse(String response)
		{
			PlaceManager.getInstance().parseJsonToPlaces(response);
			getCollectionRoute();
		}

		@Override
		public void onErrorResponse(String error)
		{
			if (error == null)
			{
				Toast.makeText(LoadingActivity.this, "网络连接超时", Toast.LENGTH_SHORT).show();
			}
			Intent intent = new Intent(LoadingActivity.this, StartActivity.class);
			startActivity(intent);
			finish();
		}
	}
	
	private class GetCollectionRouteResponseListener implements HttpResponseListener
	{

		@Override
		public void onResponse(String response)
		{
			CollectionRouteManager.getInstance().parseJsonToRoutes(response);
			mHandler.sendEmptyMessage(0x124);
		}

		@Override
		public void onErrorResponse(String error)
		{
			if (error == null)
			{
				Toast.makeText(LoadingActivity.this, "网络连接超时", Toast.LENGTH_SHORT).show();
			}
			Intent intent = new Intent(LoadingActivity.this, StartActivity.class);
			startActivity(intent);
			finish();
		}
	}
}
