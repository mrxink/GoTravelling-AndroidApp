package com.teamwork.daily;

import java.util.ArrayList;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import com.teamwork.favorite.PlaceManager;
import com.teamwork.gotravelling.R;
import com.teamwork.model.Place;
import com.teamwork.model.Sight;

public class AllPlaceFavoriteActivity extends Activity
{
	private ListView mListView;
	private PlaceAdapter mPlaceAdapter;
	
	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_all_place_favorite);
		
		showActionBar();
		
		mListView = (ListView)findViewById(R.id.lv_locations);
		mPlaceAdapter = new PlaceAdapter(PlaceManager.getInstance().getPlaces());
		mListView.setAdapter(mPlaceAdapter);
		mListView.setOnItemClickListener(new OnItemClickListener()
		{
			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int position, long arg3) 
			{
				Place place = mPlaceAdapter.getItem(position);
				Intent intent = new Intent();
				intent.putExtra(Sight.NAME, place.getName());
				intent.putExtra(Sight.LONGITUDE, place.getLongitude());
				intent.putExtra(Sight.LATITUDE,place.getLatitude());
				setResult(RESULT_OK, intent);
				finish();
			}
		});
	}
	
	private void showActionBar()
	{
		Button backButton = (Button)findViewById(R.id.btn_actionbar);
        backButton.setBackgroundResource(R.drawable.btn_back);
        backButton.setOnClickListener(new View.OnClickListener()
        {
			@Override
			public void onClick(View v) 
			{
				finish();
			}
		});
        
        TextView textView = (TextView)findViewById(R.id.tv_actionbar);
        textView.setText(R.string.place_favorite);
        
	}
	
	private class PlaceAdapter extends ArrayAdapter<Place>
	{
		public PlaceAdapter(ArrayList<Place> routes)
		{
			super(AllPlaceFavoriteActivity.this, 0, routes);
		}
		
		@Override
		public View getView(final int position, View convertView, ViewGroup parent)
		{
			if (convertView == null)
			{
				convertView = getLayoutInflater().inflate(R.layout.place_favorite_list_item, null);
			}
			
			final Place place = getItem(position);
			TextView nameTextView = (TextView)convertView.findViewById(R.id.tv_place_name);
			nameTextView.setText(place.getName());
			
			return convertView;
		}
	}
		
}
