package com.teamwork.daily;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.baidu.mapapi.map.BaiduMap;
import com.baidu.mapapi.map.MapView;
import com.baidu.mapapi.model.LatLng;
import com.baidu.mapapi.overlayutil.DrivingRouteOverlay;
import com.baidu.mapapi.overlayutil.TransitRouteOverlay;
import com.baidu.mapapi.overlayutil.WalkingRouteOverlay;
import com.baidu.mapapi.search.core.SearchResult;
import com.baidu.mapapi.search.route.DrivingRoutePlanOption;
import com.baidu.mapapi.search.route.DrivingRouteResult;
import com.baidu.mapapi.search.route.OnGetRoutePlanResultListener;
import com.baidu.mapapi.search.route.PlanNode;
import com.baidu.mapapi.search.route.RoutePlanSearch;
import com.baidu.mapapi.search.route.TransitRoutePlanOption;
import com.baidu.mapapi.search.route.TransitRouteResult;
import com.baidu.mapapi.search.route.WalkingRoutePlanOption;
import com.baidu.mapapi.search.route.WalkingRouteResult;
import com.teamwork.gotravelling.R;
import com.teamwork.utils.HandleUtil;

public class TransportShowMapActivity extends Activity 
{
	public static final String SEARCH_TYPE = "search_type";
	
	public static final String START_LONGITUDE = "start_longitude";
	public static final String START_LATITUDE = "start_latitude";
	public static final String START_NAME = "start_name";
	public static final String START_CITY = "start_city";
	
	public static final String END_LONGITUDE = "end_longitude";
	public static final String END_LATITUDE = "end_latitude";
	public static final String END_NAME = "end_name";
	public static final String END_CITY = "end_city";
	
	
	private MapView mMapView;
	private BaiduMap mBaiduMap;
	
	//路线规划的类型（0：驾车；1：公交；2：步行）
	private int mSearchType;
	
	private double mStartLongitude;
	private double mStartLatitude;
	private String mStartName;
	private String mStartCity;
	
	private double mEndLongitude;
	private double mEndLatitude;
	private String mEndName;
	private String mEndCity;
	
	//路径规划搜索
	private RoutePlanSearch mSearch;
	
	private ProgressDialog mProgressDialog;
	
	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_transport_show_map);
		handleIntent(getIntent());
		
		showActionBar();
		
		mProgressDialog = HandleUtil.showProgressDialog(this, "正在规划...");
		
		mMapView = (MapView)findViewById(R.id.mapView);
		mBaiduMap = mMapView.getMap();
		
		mSearch = RoutePlanSearch.newInstance();
		mSearch.setOnGetRoutePlanResultListener(new OnGetRoutePlanResultListener() 
		{
			@Override
			public void onGetWalkingRouteResult(WalkingRouteResult result) 
			{
				mProgressDialog.dismiss();
				
				if (result == null || result.error != SearchResult.ERRORNO.NO_ERROR) 
				{
		            Toast.makeText(TransportShowMapActivity.this, "抱歉，未找到结果", Toast.LENGTH_SHORT).show();
		        }
				if (result.error == SearchResult.ERRORNO.NO_ERROR) 
				{
		            WalkingRouteOverlay overlay = new WalkingRouteOverlay(mBaiduMap);
		            mBaiduMap.setOnMarkerClickListener(overlay);
		            overlay.setData(result.getRouteLines().get(0));
		            overlay.addToMap();
		            overlay.zoomToSpan();
				}
			}
			
			@Override
			public void onGetTransitRouteResult(TransitRouteResult result)
			{
				mProgressDialog.dismiss();
				
				if (result == null || result.error != SearchResult.ERRORNO.NO_ERROR) 
				{
		            Toast.makeText(TransportShowMapActivity.this, "抱歉，未找到结果", Toast.LENGTH_SHORT).show();
		        }
				if (result.error == SearchResult.ERRORNO.NO_ERROR) 
				{
		            TransitRouteOverlay overlay = new TransitRouteOverlay(mBaiduMap);
		            mBaiduMap.setOnMarkerClickListener(overlay);
		            overlay.setData(result.getRouteLines().get(0));
		            overlay.addToMap();
		            overlay.zoomToSpan();
		        }
			}
			
			@Override
			public void onGetDrivingRouteResult(DrivingRouteResult result) 
			{
				mProgressDialog.dismiss();
				
				if (result == null || result.error != SearchResult.ERRORNO.NO_ERROR) 
				{
		            Toast.makeText(TransportShowMapActivity.this, "抱歉，未找到结果", Toast.LENGTH_SHORT).show();
		        }
				if (result.error == SearchResult.ERRORNO.NO_ERROR) 
				{
					DrivingRouteOverlay overlay = new DrivingRouteOverlay(mBaiduMap);
		            mBaiduMap.setOnMarkerClickListener(overlay);
		            overlay.setData(result.getRouteLines().get(0));
		            overlay.addToMap();
		            overlay.zoomToSpan();
		        }
			}
		});
	
		search();
	}
	
	@Override  
	protected void onDestroy() 
	{  
		//在activity执行onDestroy时执行mMapView.onDestroy()，实现地图生命周期管理  
		mMapView.onDestroy();  
		mSearch.destroy();
		super.onDestroy();     
	}  
	
	@Override  
	protected void onResume()
	{  
	    super.onResume();  
	    //在activity执行onResume时执行mMapView. onResume ()，实现地图生命周期管理  
	    mMapView.onResume();  
	}  
	
	@Override  
	protected void onPause()
	{  
	    super.onPause();  
	    //在activity执行onPause时执行mMapView. onPause ()，实现地图生命周期管理  
	    mMapView.onPause(); 
	}  
	
	private void handleIntent(Intent intent)
	{
		mSearchType = intent.getIntExtra(SEARCH_TYPE, 0);
		
		mStartLongitude = intent.getDoubleExtra(START_LONGITUDE, 0.0);
		mStartLatitude = intent.getDoubleExtra(START_LATITUDE, 0.0);
		mStartName = intent.getStringExtra(START_NAME);
		mStartCity = intent.getStringExtra(START_CITY);
		
		mEndLongitude = intent.getDoubleExtra(END_LONGITUDE, 0.0);
		mEndLatitude = intent.getDoubleExtra(END_LATITUDE, 0.0);
		mEndName = intent.getStringExtra(END_NAME);
		mEndCity = intent.getStringExtra(END_CITY);
	}
	
	private void showActionBar()
	{
		Button backButton = (Button)findViewById(R.id.btn_actionbar);
        backButton.setBackgroundResource(R.drawable.btn_back);
        backButton.setOnClickListener(new View.OnClickListener()
        {
			@Override
			public void onClick(View v) 
			{
				finish();
			}
		});
        
        TextView textView = (TextView)findViewById(R.id.tv_actionbar);
        String title = null;
        switch (mSearchType)
        {
        case 0:
        	title = "驾车";
        	break;
        case 1:
        	title = "公交";
        	break;
        case 2:
        	title = "步行";
        	break;
        }
        
        textView.setText(title);
	}

	private void search()
	{
		mProgressDialog.show();
		
		switch (mSearchType)
	    {
		case 0:
	        drivingSearch();
	        break;
	    case 1:
	    	transitSearch();
	        break;
	    case 2:
	        walkingSearch();
	        break;
	    }
	}
	
	//驾车搜索
	private void drivingSearch()
	{
		DrivingRoutePlanOption option = new DrivingRoutePlanOption();
		option.from(getStart()).to(getEnd());
		
		mSearch.drivingSearch(option);
	}
	
	//公交搜索
	private void transitSearch()
	{
		TransitRoutePlanOption option = new TransitRoutePlanOption();
		option.from(getStart()).to(getEnd()).city("全国");
		
		mSearch.transitSearch(option);
	}
	
	//步行搜索
	private void walkingSearch()
	{
		mSearch.walkingSearch((new WalkingRoutePlanOption())
                .from(getStart())
                .to(getEnd()));
	}
	
	//获取起点
	private PlanNode getStart()
	{
		if (mStartLongitude != 0.0 && mStartLatitude != 0.0)
		{
			LatLng latlng = new LatLng(mStartLatitude, mStartLongitude);
			return PlanNode.withLocation(latlng);
		}
		else
		{
			if (mStartCity != null)
			{
				return PlanNode.withCityNameAndPlaceName(mStartCity, mStartName);
			}
			else
			{
				return PlanNode.withCityNameAndPlaceName("全国", mStartName);
			}
		}
	}
	
	//获取终点
	private PlanNode getEnd()
	{
		if (mEndLongitude != 0.0 && mEndLatitude != 0.0)
		{
			LatLng latlng = new LatLng(mEndLatitude, mEndLongitude);
			return PlanNode.withLocation(latlng);
		}
		else
		{
			if (mEndCity != null)
			{
				return PlanNode.withCityNameAndPlaceName(mEndCity, mEndName);
			}
			else
			{
				return PlanNode.withCityNameAndPlaceName("全国", mEndName);
			}
		}
	}
}
