package com.teamwork.model;

import org.json.JSONException;
import org.json.JSONObject;

import com.teamwork.utils.HandleUtil;

public class Route
{

	public static final String ID = "_id";
	public static final String NAME = "name";
	public static final String DESCRIPTION = "description";
	public static final String STATUS = "status";
	public static final String CREATED_AT = "created_at";
	public static final String TAG = "tag";
	public static final String TAG_NAME = "name";
	public static final String TAG_LABEL = "label";
	
	private String mId;
	private String mName;
	private String mDescription;
	private String mStatus;
	private String mCreatedAt;
	private String mTagName;
	private String mTagLabel;
	
	public Route clone()
	{
		Route temp = new Route();
		temp.mId = mId;
		temp.mName = mName;
		temp.mDescription = mDescription;
		temp.mStatus = mStatus;
		return temp;
	}
	
	public Route()
	{
		
	}
	
	public Route(JSONObject jsonObject)
	{
		try
		{
			mId = jsonObject.getString(Route.ID);
			mName = jsonObject.getString(Route.NAME);
			if (jsonObject.has(Route.DESCRIPTION))
			{
				mDescription = jsonObject.getString(Route.DESCRIPTION);
			}
			mStatus = jsonObject.getString(Route.STATUS);
			mCreatedAt = jsonObject.getString(Route.CREATED_AT);
		}
		catch (JSONException e)
		{
			e.printStackTrace();
		}
	}
	
	public JSONObject toJSON()
	{
		try
		{
			JSONObject jsonObject = new JSONObject();
			jsonObject.put(ID, mId);
			if (HandleUtil.isEmpty(mName))
				mName = "δ����·��";
			jsonObject.put(NAME, mName);
			jsonObject.put(DESCRIPTION, mDescription);
			jsonObject.put(STATUS, mStatus);
			return jsonObject;
		}
		catch (JSONException e)
		{
			e.printStackTrace();
			return null;
		}
	}
	
	public String getId() 
	{
		return mId;
	}
	
	public void setId(String id) 
	{
		mId = id;
	}
	
	public String getName() 
	{
		return mName;
	}
	
	public void setName(String name) 
	{
		mName = name;
	}
	
	public String getDescription()
	{
		return mDescription;
	}
	
	public void setDescription(String description) 
	{
		mDescription = description;
	}
	
	public String getStatus() 
	{
		return mStatus;
	}
	
	public void setStatus(String status) 
	{
		mStatus = status;
	}
	
	public String getCreatedAt()
	{
		return mCreatedAt;
	}
	
	public void setCreateAt(String createdAt)
	{
		mCreatedAt = createdAt;
	}

	public String getTagName()
	{
		return mTagName;
	}

	public void setTagName(String tagName)
	{
		mTagName = tagName;
	}

	public String getTagLabel()
	{
		return mTagLabel;
	}

	public void setTagLabel(String tagLabel) 
	{
		mTagLabel = tagLabel;
	}
	
	
}
