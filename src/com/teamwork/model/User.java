package com.teamwork.model;

import android.graphics.Bitmap;

public class User 
{
	public static final String USERNAME = "username";
	public static final String PASSWORD = "password";
	public static final String CELL_PHONE_NUMBER = "cellphone_number";
	public static final String NICKNAME = "nickname";
	public static final String HEAD_IMAGE = "head_image";
	public static final String SEX = "sex";
	public static final String IS_SAVE = "is_save";
	public static final String EMAIL = "email";
	public static final String SIGNATURE = "description";
	
	private String mUsername; //用户名
	private String mNickname; //昵称
	private String mCellPhone; //电话号码
	private Bitmap mHeadImage; //用户头像
	private String mSex; //性别
	private String mSignature; //个人签名
	private String mEmail; //个人邮箱
	
	public String getUsername() 
	{
		return mUsername;
	}
	
	public void setUsername(String username) 
	{
		mUsername = username;
	}
	public String getNickname() 
	{
		return mNickname;
	}
	
	public void setNickname(String nickname)
	{
		mNickname = nickname;
	}
	
	public String getCellPhone()
	{
		return mCellPhone;
	}
	
	public void setCellPhone(String cellPhone) 
	{
		mCellPhone = cellPhone;
	}
	
	public Bitmap getHeadImage() 
	{
		return mHeadImage;
	}
	
	public void setHeadImage(Bitmap headImage)
	{
		mHeadImage = headImage;
	}
	
	public String getSex()
	{
		return mSex;
	}
	
	public void setSex(String sex)
	{
		mSex = sex;
	}

	public String getSignature()
	{
		return mSignature;
	}

	public void setSignature(String signature)
	{
		mSignature = signature;
	}

	public String getEmail() 
	{
		return mEmail;
	}

	public void setEmail(String email) 
	{
		mEmail = email;
	}	
}
