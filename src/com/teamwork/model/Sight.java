package com.teamwork.model;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class Sight
{
	public static final String ID = "_id";
	public static final String PROVINCE = "province";
	public static final String CITY = "city";
	public static final String LOC = "loc";
	public static final String COORDINATES = "coordinates";
	public static final String NAME = "name";
	public static final String DESCRIPTION = "description";
	public static final String ADDRESS = "address";
	public static final String UID = "uid";
	public static final String LONGITUDE = "longitude";
	public static final String LATITUDE = "latitude";
	
	private String mId;
	private String mProvince;
	private String mCity;
	private double mLongitude;
	private double mLatitude;
	private String mName;
	private String mDescription;
	private String mAddress;
	private String mUid;
	
	public Sight()
	{
		
	}
	
	public Sight(JSONObject jsonObject)
	{
		try 
		{
			mId = jsonObject.getString(ID);
			mProvince = jsonObject.getString(PROVINCE);
			mCity = jsonObject.getString(CITY);
			
			JSONObject loc = jsonObject.getJSONObject(LOC);
			JSONArray jsonArray = loc.getJSONArray(COORDINATES);
			mLongitude = jsonArray.getDouble(0);
			mLatitude = jsonArray.getDouble(1);
			
			mName = jsonObject.getString(NAME);
			mDescription = jsonObject.getString(DESCRIPTION);
			mAddress = jsonObject.getString(ADDRESS);
		}
		catch (JSONException e)
		{
			e.printStackTrace();
		}
		
	}
	
	public JSONObject toJSON()
	{
		try
		{
			JSONObject jsonObject = new JSONObject();
			jsonObject.put(ID, mId);
			jsonObject.put(PROVINCE, mProvince);
			jsonObject.put(CITY, mCity);
			
			JSONObject loc = new JSONObject();
			loc.put("type", "Point");
			JSONArray jsonArray = new JSONArray();
			jsonArray.put(0, mLongitude);
			jsonArray.put(1, mLatitude);
			loc.put(COORDINATES, jsonArray);
			jsonObject.put(LOC, loc);
			
			jsonObject.put(NAME, mName);
			jsonObject.put(DESCRIPTION, mDescription);
			jsonObject.put(ADDRESS, mAddress);
			return jsonObject;
		}
		catch (JSONException e)
		{
			e.printStackTrace();
			return null;
		}
	}
	
	public String getId()
	{
		return mId;
	}
	
	public void setId(String id) 
	{
		mId = id;
	}
	
	public String getProvince() 
	{
		return mProvince;
	}
	
	public void setProvince(String province) 
	{
		mProvince = province;
	}
	
	public String getCity()
	{
		return mCity;
	}
	
	public void setCity(String city) 
	{
		mCity = city;
	}
	
	public double getLongitude() 
	{
		return mLongitude;
	}
	
	public void setLongitude(double longitude)
	{
		mLongitude = longitude;
	}
	
	public double getLatitude() 
	{
		return mLatitude;
	}
	
	public void setLatitude(double latitude) 
	{
		mLatitude = latitude;
	}
	
	public String getName()
	{
		return mName;
	}
	
	public void setName(String name) 
	{
		mName = name;
	}
	
	public String getDescription()
	{
		return mDescription;
	}
	
	public void setDescription(String description)
	{
		mDescription = description;
	}
	
	public String getAddress() 
	{
		return mAddress;
	}
	
	public void setAddress(String address) 
	{
		mAddress = address;
	}
	
	public void setUid(String uid)
	{
		mUid = uid;
	}
	
	public String getUid()
	{
		return mUid;
	}
}
